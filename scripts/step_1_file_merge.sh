REFFILE="./ref/*"
TESTFILE="./test/*"
python3 mergePhysValFiles.py -i $REFFILE -o files_merged/merged_NTUP_PHYSVAL_ref.root -d BTag
python3 mergePhysValFiles.py -i $TESTFILE -o files_merged/merged_NTUP_PHYSVAL_test.root -d BTag