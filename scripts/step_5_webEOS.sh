JIRA=$1
Task=$2
MC=$3

cd /afs/cern.ch/atlas/groups/validation/Btagging/
python3 CreateDB.py --folder_path /afs/cern.ch/atlas/groups/validation/Btagging/ATLPHYSVAL-${JIRA}/Task${Task}/${MC} --JIRA ATLPHYSVAL-${JIRA} --Task Task${Task} --DSID ${MC} --Domain Btagging --Status Green
# We are not sure if it is Green, just write green for the first submit
cd -
