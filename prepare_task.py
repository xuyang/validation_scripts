import os
import sys

DSID = {"ttbar": 601229, "Zprime": 801271}

# setup task parameters, change everytime here

job_suffix = "25-07-24"  # day-month-year

taskA = {
    "task_suffix": "A",
    "process": ["ttbar", "Zprime"],
    "JIRA": "ATLPHYSVAL-1082",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14909_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15002_p5744_p5745_p5746",
}

taskB = {
    "task_suffix": "B",
    "process": ["ttbar", "Zprime"],
    "JIRA": "ATLPHYSVAL-1056",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14908_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15004_p5744_p5745_p5746",
}

taskC = {
    "task_suffix": "C",
    "process": ["ttbar", "Zprime"],
    "JIRA": "ATLPHYSVAL-1079",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14908_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15004_p5744_p5745_p5746",
}

taskD = {
    "task_suffix": "D",
    "process": ["ttbar"],
    "JIRA": "ATLPHYSVAL-1080",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14908_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15004_p5744_p5745_p5746",
}

taskE = {
    "task_suffix": "E",
    "process": ["ttbar"],
    "JIRA": "ATLPHYSVAL-1060",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14908_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15004_p5744_p5745_p5746",
}

taskF = {
    "task_suffix": "F",
    "process": ["ttbar"],
    "JIRA": "ATLPHYSVAL-1058",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14908_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15004_p5744_p5745_p5746",
}
taskG = {
    "task_suffix": "G",
    "process": ["ttbar"],
    "JIRA": "ATLPHYSVAL-1059",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14908_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15004_p5744_p5745_p5746",
}
taskH = {
    "task_suffix": "H",
    "process": ["ttbar"],
    "JIRA": "ATLPHYSVAL-1062",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14908_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15004_p5744_p5745_p5746",
}
taskI = {
    "task_suffix": "I",
    "process": ["ttbar"],
    "JIRA": "ATLPHYSVAL-1063",
    "scope_tag_ref": "valid1",
    "scope_tag_test": "valid1",
    "reco_tag_ref": "a908_s4114_r14908_p5744_p5745_p5746",
    "reco_tage_test": "a912_s4114_r15004_p5744_p5745_p5746",
}

# taskC ....

task_list = [taskA, taskB, taskC, taskD]

# ----------------------------- setup finished ------------------------------
# ----------------------------- Create and copying files --------------------

current_folder = os.getcwd()
print("Working in directory: {}".format(current_folder))

job_folder = "Rel22_" + job_suffix
print("Creating job directory: {}".format(job_folder))
if os.path.isdir(job_folder):
    print("Target folder already exsits, please check. Exiting...")
    sys.exit(-1)

os.mkdir(job_folder)
os.chdir(job_folder)

for task in task_list:
    task_suffix = task["task_suffix"]
    task_name = "Task" + task_suffix
    process_list = task["process"]
    jira_folder = task["JIRA"]
    if os.path.isdir(jira_folder):
        print(
            "JIRA folder {} already created, will just use the existing one".format(
                jira_folder
            )
        )
    else:
        os.mkdir(jira_folder)
    os.chdir(jira_folder)
    print("-" * 30)
    print("Creating directory for {}".format(task_name))
    os.mkdir(task_name)
    os.chdir(task_name)
    for process in process_list:
        if process not in ["ttbar", "Zprime"]:
            print("Unknown process {}".format(process))
            continue
        print("Creating directory for {}".format(process))
        os.mkdir(str(DSID[process]))
        os.chdir(str(DSID[process]))
        print("copying files to {}/{}/{}".format(jira_folder, task_name, process))
        os.popen("source ../../../../setup_dir_{}.sh".format(process))
        os.chdir("..")
    os.chdir("../..")

print("Job directories setup done!")
