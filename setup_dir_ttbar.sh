mkdir ref test plots ROC_VR ROC_PFlow files_merged ROC_VR/eff_vs_Lxy ROC_VR/eff_vs_pt_ttbar ROC_PFlow/eff_vs_Lxy ROC_PFlow/eff_vs_pt_ttbar
cp ../../../../scripts/ttbar/* .
cp ../../../../scripts/CreatePhysValWebPage.py .
cp ../../../../scripts/add_line_breaks_to_html.sh .
cp ../../../../scripts/mergePhysValFiles.py .
cp ../../../../scripts/step* .
cp ../../../../run_steer.sh .
