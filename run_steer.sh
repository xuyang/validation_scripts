#!/bin/bash
echo "The JIRA number is: $1"
echo "The Task Letter is: $2"
echo "The sample DSID is: $3"

echo "**************** MERGING FILES ****************"
source step_1_file_merge.sh

echo "**************** CREATING WEB DISPLAY ****************"
source step_2_web_display.sh

echo "**************** CREATING ROC CURVES ****************"
source step_3_roc.sh

echo "**************** COPYING FILES ****************"
source step_4_copy_files.sh $1 $2 $3

echo "**************** CREATING WEBEOS DATABASE ****************"
source step_5_webEOS.sh $1 $2 $3
